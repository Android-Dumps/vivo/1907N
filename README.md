## full_k68v1_64-user 11 RP1A.200720.012 eng.compil.20211013.100506 release-keys
- Manufacturer: vivo
- Platform: mt6768
- Codename: 1907N
- Brand: vivo
- Flavor: full_k68v1_64-user
- Release Version: 11
- Kernel Version: 4.14.186
- Id: RP1A.200720.012
- Incremental: eng.compil.20211013.100506
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 480
- Fingerprint: vivo/1907N/1907N:11/RP1A.200720.012/compiler1013100207:user/release-keys
- OTA version: 
- Branch: full_k68v1_64-user-11-RP1A.200720.012-eng.compil.20211013.100506-release-keys
- Repo: vivo/1907N
