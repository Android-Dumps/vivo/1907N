#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_1907N.mk

COMMON_LUNCH_CHOICES := \
    omni_1907N-user \
    omni_1907N-userdebug \
    omni_1907N-eng
