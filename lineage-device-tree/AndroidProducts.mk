#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_1907N.mk

COMMON_LUNCH_CHOICES := \
    lineage_1907N-user \
    lineage_1907N-userdebug \
    lineage_1907N-eng
