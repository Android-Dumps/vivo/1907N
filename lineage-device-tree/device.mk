#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    init.vivo.fingerprint_restart_counter.sh \
    init.vivo.fingerprint.sh \
    init.vivo.crashdata.sh \
    zramsize_reconfig.sh \
    install-recovery.sh \
    init.vivo.nfc.sh \

PRODUCT_PACKAGES += \
    fstab.enableswap \
    init.mt6768.usb.rc \
    init.aee.rc \
    init.sensor_1_0.rc \
    multi_init.rc \
    init.factory.rc \
    meta_init.project.rc \
    meta_init.connectivity.rc \
    init.ago.rc \
    meta_init.modem.rc \
    init.mt6768.rc \
    factory_init.rc \
    meta_init.rc \
    factory_init.project.rc \
    factory_init.connectivity.rc \
    init.connectivity.rc \
    init.project.rc \
    init.modem.rc \
    init.recovery.wifi.rc \
    init.recovery.platform.rc \
    init.recovery.mt6768.rc \
    init.recovery.touch.rc \
    init.recovery.svc.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/vivo/1907N/1907N-vendor.mk)
